import QtQuick 2.0
import Sailfish.Silica 1.0
import CreateRoomBackend 1.0

Dialog {
    id: page

    allowedOrientations: Orientation.All

    property string room_name_text;
    property string topic_text;
    property string alias_text;
    property bool e2ee_enabled;

    CreateRoomBackend {
        id: backend

        onCreated: {
            pageStack.pop(undefined, PageStackAction.Immediate)
            pageStack.pop(undefined, PageStackAction.Immediate)
            pageStack.push("Messages.qml", { room_id: room_id, room_name: room_name_text })
        }
    }

    Component {
        id: loading
        Page {
            PageBusyIndicator {
                running: true
            }
        }
    }

    onAccepted: {
        if (vis.currentIndex == 1) {
            backend.create_public(alias_text, room_name_text, topic_text)
        } else {
            backend.create_private(room_name_text, topic_text, e2ee_enabled)
        }

        pageStack.push(loading);
    }

    Column {
        id: column
        anchors.fill: parent
        spacing: Theme.paddingLarge

        DialogHeader {
            title: qsTr("Create Room")
            acceptText: qsTr("Create")
        }

        ComboBox {
            id: vis
            label: qsTr("Visibility")
            menu: ContextMenu {
                MenuItem { text: qsTr("Private") }
                MenuItem { text: qsTr("Public") }
            }

            onValueChanged:  {
                if (currentIndex == 1) {
                    e2ee.checked = false
                }
            }
        }

        TextField {
            label: qsTr("Name")
            placeholderText: qsTr("Name")
            id: room_name
            anchors.left: parent.left
            anchors.right: parent.right
            onTextChanged: room_name_text = text
        }

        TextField {
            label: qsTr("Topic")
            placeholderText: qsTr("Topic (optional)")
            id: topic
            anchors.left: parent.left
            anchors.right: parent.right
            onTextChanged: topic_text = text
        }

        Row {
            id: room_alias
            visible: vis.currentIndex == 1
            width: parent.width
            spacing: 0

            Label {
                text: "#"
                id: hash
                leftPadding: Theme.horizontalPageMargin
                rightPadding: 0
            }

            TextField {
                label: qsTr("Room alias")
                placeholderText: qsTr("Room alias")
                width: parent.width - hash.width - hs.width
                id: r_alias
                onTextChanged: alias_text = text
            }

            Label {
                text: ":" + backend.hs()
                id: hs
                rightPadding: Theme.horizontalPageMargin
                leftPadding: 0
            }
        }


        TextSwitch {
            visible: vis.currentIndex == 0
            text: qsTr("Enable end-to-end encryption")
            description: qsTr("Bridges and most bots won't work yet.")
            id: e2ee
            onCheckedChanged: e2ee_enabled = checked
        }
    }
}
