#ifndef ROOMDIRECTORYBACKEND_H
#define ROOMDIRECTORYBACKEND_H

#include <QObject>
#include <QNetworkAccessManager>
#include <Sailfish/Secrets/secretmanager.h>
#include "roomdirectorymodel.h"


class RoomDirectoryBackend : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool searching READ searching NOTIFY searchingChanged)
    Q_PROPERTY(RoomDirectoryModel* results READ results NOTIFY resultsChanged)
public:
    explicit RoomDirectoryBackend(QObject *parent = nullptr);
    ~RoomDirectoryBackend();
    Q_INVOKABLE void search(QString term, bool matrix_org);
    bool searching();
    RoomDirectoryModel* results();

signals:
    void searchingChanged();
    void resultsChanged();
private:
    QString m_access_token;
    QString m_hs_url;
    Sailfish::Secrets::SecretManager m_secretManager;
    QNetworkAccessManager* searcher;
    QNetworkAccessManager* avatar_downloader;
    bool m_searching;
    RoomDirectoryModel* m_model;
private slots:
    void processResults(QNetworkReply* reply);
    void processAvatar(QNetworkReply* reply);
};

#endif // ROOMDIRECTORYBACKEND_H
